## M3105

dépôt contenant les fichiers de configuration du module M3105

### utilisation
1. Il faut cloner le dépot: `https://gitlab.com/trazafin/m3105.git`
2. Se positionner dans le répertoire m3105: `cd m3105`
3. Lancer le script: `deploy.sh PC1` pour configurer le PC1
