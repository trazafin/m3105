#!/usr/bin/env bash

DIR=$(pwd)  
DATE=$(date '+%Y%m%d_%H%M%S')

if [ $EUID -ne 0 ]; then
	echo "You must be root to run the script"
	exit 1
fi

if [ $# -eq 0 ]; then
	echo "usage: $0 {PC1, PC2, PC3}"
	exit 2
fi

if [ “$1” == “PC1” ]; then
	echo "Configuring PC1..."
	#mv /etc/resolv.conf /etc/resolv.conf.${DATE}
	#ln -s ${DIR}/resolv.conf /etc/resolv.conf

	#mv /etc/network/interfaces /etc/network/interfaces.${DATE}
	#ln -s ${DIR}/interfaces /etc/network/interfaces
fi
